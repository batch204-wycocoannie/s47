//documents refers to the html file linked to this JS file
//query selector al;lows us to desigante an html taht we want to access via its id or class name
//use # for ids, and. for classes

let inputFirstName = document.querySelector('#input-first-name');
let inputLastName = document.querySelector('#input-last-name');
let spanFullName = document.querySelector('#span-full-name');

//other ways to access html elements
//document.getElementByID('txt-first-name');
//document.getElementByClassName('txt-inputs');
//document.getElementByTagName('input');


//console.log(input1)
//console.dir(input1);
//input1.value="jack";
//console.dir(input1.value);

//console.dir(span1.txtContent)  //ALL text, uincluding whitespaces
//console.dir(span1.innerHTML)  //all html and text within the given html element
//console.dir(span1.outertext)
//console.dir(span1.innertext)  //just a text

//events and event listeners:
//an event is any kind of user intercation with a specific element on a web page (clicking a button, typing in an input, etc.)
//events have targets, which are the specific elements being interacted with 

//if(false){console.log("hello")}

//function sayHello(){console.log("hello")} //kung kailang gusto mangyari

//let button = document.getElementByID('button')

//an event listenet waits foir a given event to occur before code execution
//button.addEventListener('click', () => {alert("You Clicked it")})

//input1.addEventListener('keyup', (e) => {
//	console.log(e.target.value);  //other way
	
//})

//input1.addEventListener('keyup', () => {
//	console.log(event.target.value);  //more flexible it will works in any //and all inputs
//	console.log(input1.value);
//})

//input1.addEventListener('keyup', () => {
// 	myFunc()
//})




//function myFunc(){
//	console.log(event.target.value);
//}


//directions:
//listen to an event when a user types in the first and last name inputs
//when this event triger, update the span-full-name;s content to show the value of the first name input on the left andf the value of the last name input on  the right

// stretch goal: instead 

const FullName = (e) => {
	spanFullName.innerHTML = `${inputFirstName.value} ${inputLastName}`;
};

inputFirstName.addEventListener('keyup', spanFullname);
inputLastName.addEventListener('keyup', spanFullname);
