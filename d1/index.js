//documents refers to the html file linked to this JS file
//query selector al;lows us to desigante an html taht we want to access via its id or class name
//use # for ids, and. for classes

let input1 = document.querySelector('#txt-first-name');
let span1 = document.querySelector('#span-full-name');

//other ways to access html elements
//document.getElementByID('txt-first-name');
//document.getElementByClassName('txt-inputs');
//document.getElementByTagName('input');


//console.log(input1)
//console.dir(input1);
//input1.value="jack";
//console.dir(input1.value);

//console.dir(span1.txtContent)  //ALL text, uincluding whitespaces
//console.dir(span1.innerHTML)  //all html and text within the given html element
//console.dir(span1.outertext)
//console.dir(span1.innertext)  //just a text

//events and event listeners:
//an event is any kind of user intercation with a specific element on a web page (clicking a button, typing in an input, etc.)
//events have targets, which are the specific elements being interacted with 

//if(false){console.log("hello")}

//function sayHello(){console.log("hello")} //kung kailang gusto mangyari

//let button = document.getElementByID('button')

//an event listenet waits foir a given event to occur before code execution
//button.addEventListener('click', () => {alert("You Clicked it")})

input1.addEventListener('keyup', (e) => {
	console.log(e.target.value);  //other way
	
})

input1.addEventListener('keyup', () => {
	console.log(event.target.value);  //more flexible it will works in any and all inputs
	console.log(input1.value);
})

input1.addEventListener('keyup', () => {
 	myFunc()
})




function myuFunc(){
	console.log(event.target.value);
}


//activity solution


let txtFirstName= document.querySelector('#txt-first-name');
let txtFirstName= document.querySelector('#txt-last-name');
let spanFullName= document.querySelector('#span-full-name');

const updateFullname =() => {
	let firstName = txtFirstName.value
	let lastName = txtLastName.value

	spanFullName.innerHTML = `${firstName} ${lastName}`

}

txtFirstName.addEventListener('keyup', () => {
	updateFullname()
})

txtLastName.addEventListener('keyup', updateFullname)

